﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Serializers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly ParkingService _parkingService;
        public VehiclesController(ParkingService service)
        {
            _parkingService = service;
        }

        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> GetVehicles()
        {
            return _parkingService.GetVehicles();
        }

        [HttpPost]
        public ActionResult<string> AddVehicle([FromBody] VehicleSerialize _vehicle)
        {
            try
            {
            string _id = _vehicle.Id;
            decimal _balance = _vehicle.Balance;
            int vehicleType = _vehicle.VehicleType;
            VehicleType _vehicleType = (VehicleType)Enum.GetValues(typeof(VehicleType)).GetValue(vehicleType);
            _parkingService.AddVehicle(new Vehicle(_id, _vehicleType, _balance));
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is IndexOutOfRangeException)
                {
                    var message = JsonConvert.SerializeObject("Body is invalid");
                    return BadRequest(message);
                }
            }
            return new ObjectResult(JsonConvert.SerializeObject(_vehicle)) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpGet("{id}")]
        public ActionResult<string> GetVehicleById(string id)
        {
            if(!Vehicle.IdVerification(id))
            {
                var message = JsonConvert.SerializeObject("ID format is invalid");
                return BadRequest(message);
            }
            Vehicle vehicle = (Vehicle)_parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if(vehicle == null)
            {
                var message = JsonConvert.SerializeObject("Vehicle with this ID was not found");
                return NotFound(message);
            }
            else
            {
                return JsonConvert.SerializeObject(vehicle);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicleById(string id)
        {
            try
            {
                if (!Vehicle.IdVerification(id))
                {
                    var message = JsonConvert.SerializeObject("ID format is invalid");
                    return BadRequest(message);
                }
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                var message = JsonConvert.SerializeObject("Vehicle with thid ID was not found");
                return NotFound(message);
            }

        }
    }
}