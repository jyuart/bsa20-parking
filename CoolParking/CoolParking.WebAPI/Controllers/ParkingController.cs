﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using CoolParking.WebAPI;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly ParkingService _parkingService;
        public ParkingController(ParkingService service)
        {
            _parkingService = service;
        }
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return _parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public ActionResult<decimal> GetCapacity()
        {
            return _parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public ActionResult<decimal> GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }
    }
}