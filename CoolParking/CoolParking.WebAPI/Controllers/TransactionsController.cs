﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Serializers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly ParkingService _parkingService;
        private readonly LogService _logService;
        public TransactionsController(ParkingService parkingService, LogService logService)
        {
            _parkingService = parkingService;
            _logService = logService;
        }

        [HttpGet("last")]
        public ActionResult<string> GetLastTransactions()
        {
            var json = JsonConvert.SerializeObject(_parkingService.GetLastParkingTransactions());
            return json;
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransaction()
        {
            try
            {
                return _logService.Read();
            }
            catch (InvalidOperationException)
            {
                var message = JsonConvert.SerializeObject("File with logs was not found");
                return NotFound(message);
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] ToppingSerializer _vehicle)
        {
            try
            {
                if (!Vehicle.IdVerification(_vehicle.Id) || _vehicle.Sum < 0)
                {
                    var message = JsonConvert.SerializeObject("Request body is invalid");
                    return BadRequest(message);
                }
                _parkingService.TopUpVehicle(_vehicle.Id, _vehicle.Sum);
                return _parkingService.GetVehicles().First(x => x.Id == _vehicle.Id);
            }
            catch (ArgumentException)
            {
                var message = JsonConvert.SerializeObject("Vehicle with this ID was not found");
                return NotFound(message);
            }
        }
    }
}