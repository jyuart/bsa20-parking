﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Linq;
using CoolParking.BL;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Timers;
using System.Reflection;
using System.IO;

namespace CoolParking.BL
{
    class Program
    {
        static void Main(string[] args)
        {
            string _logPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            TimerService _withdrawTimer = new TimerService();
            TimerService _logTimer = new TimerService();
            LogService _logService = new LogService(_logPath);
            ParkingService _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);
            string line;
            do
            {
                Variants();
                line = Console.ReadLine();
                switch (line)
                {
                    case "1":
                        Console.WriteLine($"Текущий баланс парковки: {_parkingService.GetBalance()}");
                        Continue();
                        break;
                    case "2":
                        Console.WriteLine($"Парковка заработала (за последние 60 секунд или меньше): {_parkingService.GetLastParkingTransactions().Sum(tr => tr.Sum)}");
                        Continue();
                        break;
                    case "3":
                        Console.WriteLine($"Свободных мест на парковке: {_parkingService.GetFreePlaces()}");
                        Continue();
                        break;
                    case "4":
                        foreach (TransactionInfo transaction in _parkingService.GetLastParkingTransactions())
                        {
                            Console.WriteLine($"{transaction.Time.ToString()}\nID: {transaction.Id}\nСумма: {transaction.Sum.ToString()}");
                        };
                        Continue();
                        break;
                    case "5":
                        Console.WriteLine(_logService.Read());
                        Continue();
                        break;
                    case "6":
                        int i = 1;
                        foreach (Vehicle vehicle in _parkingService.GetVehicles())
                        {

                            Console.WriteLine($"Транспортное средство №{i++}\n{vehicle.Id}\nТип: {vehicle.VehicleType.ToString()}\nБаланс: {vehicle.Balance.ToString()}\n");
                        }
                        Continue();
                        break;
                    case "7":
                        _parkingService.AddVehicle(Adding());
                        Continue();
                        break;
                    case "8":
                        Console.WriteLine("Укажите ID автомобиля, который нужно удалить в формате ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ).");
                        string idToDelete = Console.ReadLine();
                        _parkingService.RemoveVehicle(idToDelete);
                        Continue();
                        break;
                    case "9":
                        Console.WriteLine("Укажите ID автомобиля, который нужно удалить в формате ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ).");
                        string idToTopUp = Console.ReadLine();
                        Console.WriteLine("Укажите сумму пополнения");
                        string sumToTopUp = Console.ReadLine();
                        decimal decimalSum = decimal.Parse(sumToTopUp);
                        _parkingService.TopUpVehicle(idToTopUp, decimalSum);
                        Continue();
                        break;
                }
            } while (line != "q");
        }
        public static void Variants()
        {
            Console.WriteLine("1. Вывести на экран текущий баланс Парковки");
            Console.WriteLine("2. Вывести на экран сумму заработанных денег за текущий период (до записи в лог)");
            Console.WriteLine("3. Вывести на экран количество свободных / занятых мест на парковке");
            Console.WriteLine("4. Вывести на экран все Транзакции Парковки за текущий период (до записи в лог)");
            Console.WriteLine("5. Вывести историю транзакций (считав данные из файла Transactions.log)");
            Console.WriteLine("6. Вывести на экран список Тр.средств находящихся на Паркинге");
            Console.WriteLine("7. Поcтавить тр. средство на Паркинг");
            Console.WriteLine("8. Забрать транспортное средство с Паркинга");
            Console.WriteLine("9. Пополнить баланс конкретного тр. средства.\n");
            Console.WriteLine("Что хотите сделать?");
        }

        public static Vehicle Adding()
        {
            Console.WriteLine("Введите ID автомобиля в формате ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ).");
            string currentId = Console.ReadLine();
            Console.WriteLine("Выберите тип автомобиля:\n1. Пассажирское авто\n2. Грузовая машина\n3. Автобус\n4. Мотоцикл\n");
            string typeInput = Console.ReadLine();
            VehicleType currentType = VehicleType.None;
            switch (typeInput)
            {
                case "1":
                    currentType = VehicleType.PassengerCar;
                    break;
                case "2":
                    currentType = VehicleType.Truck;
                    break;
                case "3":
                    currentType = VehicleType.Bus; 
                    break;
                case "4":
                    currentType = VehicleType.Motorcycle;
                    break;
            }
            Console.WriteLine("Укажите баланс автомобиля\n");
            string balanceInput = Console.ReadLine();
            int currentBalance;
            int.TryParse(balanceInput, out currentBalance);
            Vehicle vehicle = new Vehicle(currentId, currentType, currentBalance);
            return vehicle;
        }

        public static void Continue()
        {
            Console.WriteLine("Нажмите любую кнопку, чтобы продолжить...");
            Console.ReadLine();
        }


    public bool InputVerification(string Command)
        {
            int n;
            bool isNumeric = int.TryParse("string", out n);
            return isNumeric;
        }
    }
}
