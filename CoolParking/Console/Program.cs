﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Serializers;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace CoolParking.BL
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();
        static async Task Main()
        {
            // Asking for user input
            string answer;
            do
            {
                Menu();
                answer = Console.ReadLine();
                switch (answer)
                {
                    case "1":
                        await GetBalance();
                        Continue();
                        break;
                    case "2":
                        await GetLastRevenue();
                        Continue();
                        break;
                    case "3":
                        await GetFreePlaces();
                        Continue();
                        break;
                    case "4":
                        await ShowingLastTransactions();
                        Continue();
                        break;
                    case "5":
                        await GetAllTransactions();
                        Continue();
                        break;
                    case "6":
                        await GetVehicles();
                        Continue();
                        break;
                    case "7":
                        await AddVehilce(GetVehicleInfo());
                        Continue();
                        break;
                    case "8":
                        await DeleteVehicle();
                        Continue();
                        break;
                    case "9":
                        await TopUpVehicle();
                        Continue();
                        break;
                    default:
                        Console.WriteLine("Введите цифру от 1 до 9, чтобы выполнить соответствующую команду или нажмите q, чтобы закончить.");
                        Continue();
                        break;
                }
            } while (answer != "q");

        }

        // Asking for any input to continue working with program
        private static void Continue()
        {
            Console.WriteLine("Нажмите любую кнопку, чтобы продолжить...");
            Console.ReadLine();
        }

        // Main menu
        public static void Menu()
        {
            Console.WriteLine("1. Вывести на экран текущий баланс парковки");
            Console.WriteLine("2. Вывести на экран сумму заработанных денег за текущий период (до записи в лог)");
            Console.WriteLine("3. Вывести на экран количество свободных/занятых мест на парковке");
            Console.WriteLine("4. Вывести на экран все транзакции парковки за текущий период (до записи в лог)");
            Console.WriteLine("5. Вывести историю транзакций");
            Console.WriteLine("6. Вывести на экран список транспортных средств на паркинге");
            Console.WriteLine("7. Поcтавить транспортное средство на паркинг");
            Console.WriteLine("8. Забрать транспортное средство с паркинга");
            Console.WriteLine("9. Пополнить баланс конкретного тр. средства.\n");
            Console.WriteLine("Что хотите сделать? (введите q, чтобы закончить)");
        }

        // Getting parking balance
        private static async Task GetBalance()
        {
            var stringTask = client.GetStringAsync("https://localhost:5001/api/parking/balance");
            var msg = await stringTask;
            Console.WriteLine($"Текущий баланс парковки: {msg}");
        }

        // Get last transactions for displaying it and calculating recent revenue
        private static async Task<string> GetLastTransactions()
        {
            return await client.GetStringAsync("https://localhost:5001/api/transactions/last");
        }
        
        // Calculating sum of all transaction before writing to log
        private static async Task GetLastRevenue()
        {
            var Task = GetLastTransactions();
            var msg = await Task;
            var transactions = JsonConvert.DeserializeObject<TransactionInfo[]>(msg);
            var sum = transactions.Sum(tr => tr.Sum);
            Console.WriteLine($"Парковка заработала (за последние 60 секунд или меньше): {sum}");
        }

        // Get free places on pakring
        private static async Task GetFreePlaces()
        {
            var stringTask = client.GetStringAsync("https://localhost:5001/api/parking/capacity");
            var stringTask2 = client.GetStringAsync("https://localhost:5001/api/parking/freePlaces");
            int.TryParse(await stringTask, out int capacity);
            int.TryParse(await stringTask2, out int freePlaces);
            Console.WriteLine($"На парковке {freePlaces} свободных мест. Занято {capacity - freePlaces} мест.");
        }

        // Showing last transactions
        private static async Task ShowingLastTransactions()
        {
            var msg = await GetLastTransactions();
            var transactions = JsonConvert.DeserializeObject<TransactionInfo[]>(msg);
            foreach (TransactionInfo transaction in transactions)
            {
                Console.WriteLine($"{transaction.Time}: {transaction.Sum} was withdrawn from vehicle with ID {transaction.Id}\n");
            }
        }

        // Getting vehicle info to use in other methods
        private static VehicleSerialize GetVehicleInfo()
        {
            Console.WriteLine("Введите ID автомобиля в формате ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ).");
            string id = Console.ReadLine();
            Console.WriteLine("Введите тип автомобиля:\n1. Пассажирское авто\n2. Грузовая машина\n3. Автобус\n4. Мотоцикл\n");
            int.TryParse(Console.ReadLine(), out int type);
            Console.WriteLine("Укажите баланс автомобиля: ");
            decimal.TryParse(Console.ReadLine(), out decimal balance);
            VehicleSerialize vehicle = new VehicleSerialize
            {
                Id = id,
                // -1 because struct elements are indexing from zero
                VehicleType = type -1,
                Balance = balance
            };
            return vehicle;
        }

        // Adding vehicle to parking
        private static async Task AddVehilce(VehicleSerialize vehicle)
        {
            var json = JsonConvert.SerializeObject(vehicle);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            _ = await client.PostAsync("https://localhost:5001/api/vehicles", data);
            Console.WriteLine("Транспортное средство успешно добавлено");
        }

        // Getting transaction from log file
        private static async Task GetAllTransactions()
        {
            try
            {
                var stringTask = client.GetStringAsync("https://localhost:5001/api/transactions/all");
                var msg = await stringTask;
                Console.WriteLine(msg);
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Файл не найден.");
            }
        }

        // Getting all vehicles from parking
        private static async Task GetVehicles()
        {
            var stringTask = client.GetStringAsync("https://localhost:5001/api/vehicles");
            var json = await stringTask;
            var vehicles = JsonConvert.DeserializeObject<ReadOnlyCollection<Vehicle>>(json);
            Console.WriteLine($"На парковке {vehicles.Count} транспортных средств");
            foreach (Vehicle vehicle in vehicles)
            {
                Console.WriteLine($"{vehicle.VehicleType} ({vehicle.Id}) с балансом {vehicle.Balance}");
            }
        }

        // Removing vehicle from parking
        private static async Task DeleteVehicle()
        {
            Console.WriteLine("Укажите ID автомобиля, который нужно удалить в формате ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ).");
            string id = Console.ReadLine();
            await client.DeleteAsync($"https://localhost:5001/api/vehicles/{id}");
            Console.WriteLine("Транспортное средство было убрано с парковки");
        }

        // Topping up vehicle balance
        private static async Task TopUpVehicle()
        {
            Console.WriteLine("Введите ID автомобиля, баланс которого нужно пополнить.");
            var id = Console.ReadLine();
            Console.WriteLine("Введите сумму, на которую нужно пополнить баланс");
            decimal.TryParse(Console.ReadLine(), out decimal sum);
            ToppingSerializer vehicle = new ToppingSerializer
            {
                Id = id,
                Sum = sum
            };
            var json = JsonConvert.SerializeObject(vehicle);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            await client.PutAsync("https://localhost:5001/api/transactions/topUpVehicle", data);
            Console.WriteLine($"Счет транспортного средства с ID {id} был пополнен на сумму {sum}");
        }
    }
}
