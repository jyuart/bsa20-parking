﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using CoolParking.BL;
using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public struct TransactionInfo
    {
        [JsonProperty(PropertyName = "transactionDate")]
        public DateTime Time { get; set; }
        [JsonProperty(PropertyName = "vehicleId")]
        public string Id;
        [JsonProperty(PropertyName = "sum")]
        public decimal Sum;
    }
}
