﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    static class Settings
    {
        public static readonly decimal InitialBalance = 0;
        public static readonly int Capacity = 10;
        public static readonly double WithdrawPeriod = 5000;
        public static readonly int LogPeriod = 60000;
        public static readonly decimal Coefficient = 2.5m;
        public static readonly Dictionary<VehicleType, decimal> Tariffs = new Dictionary<VehicleType, decimal>
        {
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1m},
            { VehicleType.PassengerCar, 2m },
            { VehicleType.Truck, 5m }
        };
    }
}