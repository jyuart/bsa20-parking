﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;
using System.Threading;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        // Hidden constructor to prevent creating instances with new keyword
        private Parking() { }
        public static decimal ParkingBalance { get; set; }
        public static List<Vehicle> VehicleCollection = new List<Vehicle>();

        // Singleton object in private static field
        private static Parking parking = null;

        // Method for getting access to object
        public static Parking GetInstance()
        {
            if (parking == null)
                parking = new Parking();
            return parking;
        }
    }
}