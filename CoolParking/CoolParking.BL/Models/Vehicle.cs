﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using Fare;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!IdVerification(id) || balance < 0)
                throw new ArgumentException();
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
        static readonly string pattern = @"[A-Z][A-Z]-\d\d\d\d-[A-Z][A-Z]";

        // Method for checking if ID format is valid
        public static bool IdVerification(string vehicleId)
        {
            Regex regex = new Regex(pattern);
            if (regex.IsMatch(vehicleId))
                return true;
            return false;
        }

        // Automatic number ID generator
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string id;
            var xeger = new Xeger(pattern);
            id = xeger.Generate();
            return id;
        }
    }
}