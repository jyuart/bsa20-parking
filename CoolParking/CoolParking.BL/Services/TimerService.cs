﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL;
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService :  ITimerService
    {
        readonly Timer timer = new Timer();
        public event ElapsedEventHandler Elapsed
        {
            add
            {
                timer.Elapsed += value;
            }
            remove
            {
                timer.Elapsed -= value;
            }

        }
        public double Interval
        {
            get
            {
                return timer.Interval;
            }
            set
            {
                timer.Interval = value;
            }
        }
        public void Start()
        {
            timer.Start();
        }
        public void Stop()
        {
            timer.Stop();
        }
        public void Dispose() { }
    }
}