﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Transactions;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        private readonly ITimerService WithdrawTimer;
        private readonly ITimerService LogTimer;
        private readonly ILogService LogService;
        public List<TransactionInfo> LastTransactions = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.WithdrawTimer = withdrawTimer;
            this.LogTimer = logTimer;
            this.LogService = logService;
            WithdrawTimer.Interval = Settings.WithdrawPeriod;
            LogTimer.Interval = Settings.LogPeriod;
            WithdrawTimer.Elapsed += OnElapsedTransaction;
            LogTimer.Elapsed += OnElapsedLog;
            withdrawTimer.Start();
            logTimer.Start();
        }

        // Getting parking balance from its field
        public decimal GetBalance()
        {
            return Parking.ParkingBalance;
        }

        // Getting parking capacity from its field
        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        // Getting free parking places from its capacity and number of vehicles in collection
        public int GetFreePlaces()
        {
            return GetCapacity() - Parking.VehicleCollection.Count;
        }

        // Getting all vehicles from parking
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.VehicleCollection);
        }

        // Adding new vehicle to parking
        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0)
                throw new InvalidOperationException();
            if (Parking.VehicleCollection.Any(x => x.Id == vehicle.Id))
                // Throwing exception if vehicle with this ID is already in collection
                throw new ArgumentException();
            Parking.VehicleCollection.Add(vehicle);
        }
        
        // Removing vehicle from collection
        public void RemoveVehicle(string vehicleId)
        {
            try
            {
                var vehicle = Parking.VehicleCollection.Find(x => x.Id == vehicleId);
                if (vehicle.Balance < 0)
                    // Throwing exception while trying to remove vehicle with negative balance
                    throw new InvalidOperationException();
                Parking.VehicleCollection.Remove(vehicle);
            }
            catch (NullReferenceException)
            {
                // Throwing exception if this vehicle is not there
                throw new ArgumentException();
            }
        }
        
        // Topping up vehicle balance
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            try
            {
                var vehicle = Parking.VehicleCollection.Find(x => x.Id == vehicleId);
                if (sum < 0)
                {
                    // Throwing exception if number is less than 0
                    throw new ArgumentException();
                }
                vehicle.Balance += sum;
            }
            catch (NullReferenceException)
            {
                // Throwing exception if vehicle with this ID is not found
                throw new ArgumentException();
            }
        }

        // Automatic withdrawal of money from vehicle balance on parking balance
        public void FeeService()
        {
            foreach (Vehicle vehicle in Parking.VehicleCollection)
            {
                var tariff = Settings.Tariffs[vehicle.VehicleType];
                if (vehicle.Balance < 0)
                {
                    tariff *= Settings.Coefficient;
                }
                else if (vehicle.Balance < tariff)
                {
                    tariff = vehicle.Balance + (tariff - vehicle.Balance) * Settings.Coefficient;
                }
                vehicle.Balance -= tariff;
                Parking.ParkingBalance += tariff;
                LastTransactions.Add(new TransactionInfo
                {
                    Id = vehicle.Id,
                    Sum = tariff,
                    Time = DateTime.Now
                });
            }
        }

        // Getting last transactions before writing them to log file
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return LastTransactions.ToArray();
        }

        // Creating log string for writing it to log file
        public string MakeLogString(List<TransactionInfo> MyList)
        {
            string Logs = "";
            foreach (TransactionInfo trans in MyList.ToArray())
            {
                Logs += $"{trans.Time}: {trans.Sum} money was withdrawn from vehicle with ID {trans.Id}\n";
            }
            LastTransactions.Clear();
            return Logs;
        }

        // Automatic withdrawing and writing to log file on timers
        public void OnElapsedLog(Object source, ElapsedEventArgs e)
        {
            LogService.Write(MakeLogString(LastTransactions));
        }
        public void OnElapsedTransaction(Object source, ElapsedEventArgs e)
        {
            FeeService();
        }

        public void Dispose()
        {
            Parking.VehicleCollection = new List<Vehicle>();
            Parking.ParkingBalance = 0;
            File.Delete($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
        }
    }
}