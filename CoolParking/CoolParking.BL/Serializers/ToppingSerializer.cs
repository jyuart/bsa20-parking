﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Serializers
{
    // Class for deserializing body in controllers
    public class ToppingSerializer
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
